/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.hellojni;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import sourceafis.simple.AfisEngine;
import sourceafis.simple.Fingerprint;
import sourceafis.simple.Person;


public class HelloJni extends Activity {
  /**
   * Called when the activity is first created.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

        /* Create a TextView and set its content.
         * the text is retrieved by calling a native
         * function.
         */
    TextView tv = new TextView(this);
    tv.setText(stringFromJNI());
    setContentView(tv);

    createFmdFromRaw("/sdcard/test.pgm", "/sdcard/generated.ist");
    tv.setText(compareFingerprint("/sdcard/generated.ist", "/sdcard/generated.ist") + "");

  }

  /* A native method that is implemented by the
   * 'hello-jni' native library, which is packaged
   * with this application.
   */
  public native String stringFromJNI();

  /**
   * extract fingerprint feature
   *
   * @param filename       the pgm input file name
   * @param outputFilename the ISO/IEC 19794-2:2005 output file name
   */
  public native void createFmdFromRaw(String filename, String outputFilename);

  private AfisEngine afisEngine = new AfisEngine();

  /**
   * compare ISO/IEC 19794-2:2005 template
   * @param template1
   * @param template2
   * @return
   */
  public float compareFingerprint(byte[] template1, byte[] template2) {
    Person p1 = new Person();
    Fingerprint f1 = new Fingerprint();
    f1.setIsoTemplate(template1);
    p1.getFingerprints().add(f1);

    Person p2 = new Person();
    Fingerprint f2 = new Fingerprint();
    f2.setIsoTemplate(template2);
    p2.getFingerprints().add(f2);

    return afisEngine.verify(p1, p2);
  }

  public float compareFingerprint(String filename1, String filename2) {
    try {
      InputStream is = new FileInputStream(filename1);
      int size = is.available();
      byte[] b1 = new byte[size];
      is.read(b1);
      is.close();

      is = new FileInputStream((filename2));
      byte[] b2 = new byte[size];
      is.read(b2);
      is.close();

      return compareFingerprint(b1, b2);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return 0;
  }

  /* this is used to load the 'hello-jni' library on application
   * startup. The library has already been unpacked into
   * /data/data/com.example.hellojni/lib/libhello-jni.so at
   * installation time by the package manager.
   */
  static {
    System.loadLibrary("hello-jni");
  }
}
